//
//  YTTextItem.h
//  CoreTextDemo
//
//  Created by aron on 2018/7/13.
//  Copyright © 2018年 aron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YTBaseDataItem.h"

@interface YTTextItem : YTBaseDataItem
@property (nonatomic, copy) NSString *content;
@end
